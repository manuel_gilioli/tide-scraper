package com.mg.test;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TideScraper {
	//Web info
	private static final String URL="https://www.tide-forecast.com/";
	private static final String[] LOCATION = { "Half-Moon-Bay-California", "Huntington-Beach","Providence-Rhode-Island", "Wrightsville-Beach-North-Carolina" };
	private static final Integer TIME_OUT=10*1000;
	
	//Status
	private static final String SUNRISE="Sunrise";
	private static final String LOW_TIDE="Low Tide";
	private static final String SUNSET="Sunset";
	
	//Reference class style
	private static final String CLASS_DATE="date";
	private static final String CLASS_TIME_TIDE="[class=\"time tide\"]";
	private static final String CLASS_TIME_ZONE="time-zone";
	private static final String CLASS_LEVEL_METRIC="metric";
	
	

	public static void main(String[] args) throws IOException {
		String day="";
		Integer status=0;
		for(String location:LOCATION) {
			StringBuffer str= new StringBuffer();
			Document doc = Jsoup.connect(URL+"locations/"+location+"/tides/latest").timeout(TIME_OUT).validateTLSCertificates(false).get();
			Element table = doc.select("table").get(0); 
			Elements rows = table.select("tr");

			for (int i = 0; i < rows.size(); i++) {
			    Element row = rows.get(i);
			    if(row.getElementsByClass(CLASS_DATE).text().trim().compareTo("")!=0) {
			    	status=0;
			    	day=row.getElementsByClass(CLASS_DATE).text();
			    }
			    int size=row.getAllElements().size();
			    String tmp=row.getAllElements().get(size-1).getAllElements().text();
			    if(tmp.compareTo(SUNRISE)==0) {
			    	status=1;
			    }else if(tmp.compareTo(SUNSET)==0) {
			    	status=0;
			    }else if(tmp.compareTo(LOW_TIDE)==0 && status==1) {
			    	str.append(location);
			    	str.append(",");
			    	str.append(LOW_TIDE);
			    	str.append(",");
			    	str.append(day);
			    	str.append(" ");
			    	str.append(row.select(CLASS_TIME_TIDE).text());
			    	str.append(" ");
			    	str.append(row.getElementsByClass(CLASS_TIME_ZONE).text());
			    	str.append(",");
			    	str.append(row.getElementsByClass(CLASS_LEVEL_METRIC).text());
			    	System.out.println(str.toString());
			    	str.setLength(0);
			    }
			}
		}
	}
}
