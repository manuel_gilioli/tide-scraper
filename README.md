# tide-scraper


## prerequisites

bash console  
git   
java   
maven   

### My configuration

OSX   macOS High Sierra  
git   version 2.15.1  
java  oracle jdk 9  
maven version 10.13.5

## install & run

git clone https://manuel_gilioli@bitbucket.org/manuel_gilioli/tide-scraper.git  
cd tide-scraper &&  mvn package  
java -jar target/tide-scraper-1.0.0.jar  